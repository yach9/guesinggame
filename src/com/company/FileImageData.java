package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class FileImageData implements ImageDao {
    @Override
    public void add(Imageable imageable) {

    }

    @Override
    public List<Imageable> findAll() {
        return null;
    }

    @Override
    public void generateExampleData() {
        try (Stream<Path> paths = Files.walk(Paths.get("C:\\puzzle\\images"))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(path -> System.out.println(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        FileImageData imageData = new FileImageData();
        imageData.generateExampleData();
    }
}
