package com.company;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Main extends JFrame {

    private ImageDao imageDao;

    private List<Imageable> imageables = new ArrayList<>();

    public List<Imageable> getImageables() {
        return imageables;
    }

    public void setImageables(List<Imageable> imageables) {
        this.imageables = imageables;
    }

    public ImageDao getImageDao() {
        return imageDao;
    }

    public void setImageDao(ImageDao imageDao) {
        this.imageDao = imageDao;
        imageDao.generateExampleData();
    }

    private JPanel jPanel = new JPanel();

    private ImageGenerator simpleImageGenerator;


    public Main() {

        createFrameLayout();
        setImageDao(new ExampleImageData());
        simpleImageGenerator = new SimpleImageGenerator(getImageDao().findAll());

        try {
            imageables = simpleImageGenerator.generate();
        } catch (NoImageException e) {
            e.printStackTrace();
        }
        for (int i = 1; i <= 4; i++) {
            JButton jButton = new JButton("");
            ImageIcon imageIcon = new
                    ImageIcon(imageables.get(i-1).getFileName());
            jButton.setIcon(imageIcon);
            jPanel.add(jButton);
        }
        add(jPanel);

    }

    private void createFrameLayout() {
        setSize(500, 500); // ustawia wielkość okna na 500 na 500 pikseli
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // sprawia,że będzie działał exit
        setVisible(true); // widoczne okno
        setLayout(new BoxLayout(getContentPane(),BoxLayout.Y_AXIS));
        JLabel jLabel = new JLabel("Co nie jest kotem?");
        jLabel.setFont(new Font("Serif",Font.PLAIN,24));
        jLabel.setForeground(Color.red);
        add(jLabel);
        jPanel.setLayout(new GridLayout(2, 2));
    }


    public static void main(String[] args) {
        // write your code here
        // będzie w tle tworzone
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main(); // tworzenie okna
            }
        });
    }
}
